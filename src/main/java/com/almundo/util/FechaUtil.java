package com.almundo.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class FechaUtil {
	
	public static final String FORMATO_FECHA = "dd/MM/yyyy";
	public static final String FORMATO_FECHA_HORA = "dd/MM/yyyy hh:mm:ss a";
	
	public static LocalDate stringToLocalDate(String formato, String fecha) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(formato);
		LocalDate localDate = LocalDate.parse(fecha, formatter);
		return localDate;
	}
	
	public static LocalDateTime stringToLocalDateTime(String formato, String fecha) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(formato);
		LocalDateTime localDateTime = LocalDateTime.parse(fecha, formatter);
		return localDateTime;
	}
	
	public static String LocalDateToString(String formato, LocalDate fecha) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(formato);
		return fecha != null ? fecha.format(formatter) : null;
	}
	
	public static String LocalDateTimeToString(String formato, LocalDateTime fecha) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(formato);
		return fecha.format(formatter);
	}

}
