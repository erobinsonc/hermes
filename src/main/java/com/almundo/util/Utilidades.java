/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.util;

import java.util.Calendar;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 *
 * @author Emmanuel Robinson
 */
public class Utilidades {
    
    /**
     * Variable LOGGER
     * A traves de esta se puede realizar el registro en el log
     */
    private static Logger logger = LogManager.getLogger(Utilidades.class);
    
    /**
     * Variable TIME_MIN Define el tiempo minimo de una llamada
     */
    private final static int TIME_MIN = 5000;

    /**
     * Variable TIME_MAX Define el tiempo maximo de una llamada
     */
    private final static int TIME_MAX = 10000;
    
    /**
     * Constructor de la clase Utilidades
     */
    private Utilidades(){
        
    }
    
    public static Long durationCall() {
        int duracion = 0;
        try {
            duracion = ThreadLocalRandom.current().nextInt(TIME_MIN, TIME_MAX + 1);
            Thread.sleep(duracion);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        return Long.valueOf(duracion);
    }
}
