/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.cache;

import com.almundo.enums.CargoEmpleadoEnum;
import com.almundo.model.Empleado;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.ObjectUtils;

/**
 *
 * @author Emmanuel Robinson
 */
public class EmpleadoCache {

    /**
     * Variable LOGGER 
     * A traves de esta se puede realizar el registro en el log
     */
    private static Logger LOGGER = LogManager.getLogger(EmpleadoCache.class);
    
    /**
     * Instancia de la clase EmpleadoCache
     */
    private volatile static EmpleadoCache instance = null;

    /**
     * Lista de empleados que manejara la clase EmpleadoCache
     */
    private static List<Empleado> empleados;

    /**
     * Constructor de la clase 
     */
    private EmpleadoCache() {
        empleados = new ArrayList<>();
    }

    /**
     * Metodo que incializa el singleton de la clase EmpleadoCache
     * @return EmpleadoCache
     */
    public static EmpleadoCache getInstance() {
        if (instance == null) {
            synchronized (EmpleadoCache.class) {
                if (instance == null) {
                    instance = new EmpleadoCache();
                }
            }
        }
        return instance;
    }
    
    /**
     * Metodo encargado de consultar los empleados cargados
     * @return List<Empleado>
     */
    public synchronized List<Empleado> consultarEmpleados(){
        return empleados;
    }
    
    /**
     * Metodo encargado de adicionar empleados a la lista
     * @param empleado 
     */
    public synchronized void adicionarEmpleado(Empleado empleado){
        if(!ObjectUtils.isEmpty(empleado)){
            empleados.add(empleado);
        }
        
    }
    
    /**
     * Metodo encargado de cargar empleados
     * @param ListaEmpleados 
     */
    public void cargarEmpleados(List<Empleado> ListaEmpleados){
        if(!ObjectUtils.isEmpty(ListaEmpleados)){
            empleados.addAll(ListaEmpleados);
        }
    }
    
    /**
     * Metod encargado de validar disponibilidad de empleados, retornando
     * el que se encuentre esta condicicon
     * @param listaEmpleados
     * @param tipoEmpleado
     * @return Empleado
     */
    public synchronized Empleado asignarEmpleado(List<Empleado> listaEmpleados, char tipoEmpleado) {
        Empleado emp = null;
        switch (tipoEmpleado) {
            case 'O':
                for (Iterator<Empleado> iterator = listaEmpleados.iterator(); iterator.hasNext();) {
                    Empleado empleado = iterator.next();
                    if (empleado.getCargoEmpleado().equals(CargoEmpleadoEnum.OPERADOR)) {
                        if (empleado.isDisponible()) {
                            emp = empleado;
                            iterator.remove();
                            break;
                        }
                    }
                }
                if (emp != null) {
                    return emp;
                } else {
                    return asignarEmpleado(listaEmpleados, 'S');
                }
            case 'S':
                for (Iterator<Empleado> iterator = listaEmpleados.iterator(); iterator.hasNext();) {
                    Empleado empleado = iterator.next();
                    if (empleado.getCargoEmpleado().equals(CargoEmpleadoEnum.SUPERVISOR)) {
                        if (empleado.isDisponible()) {
                            emp = empleado;
                            iterator.remove();
                            break;
                        }
                    }
                }
                if (emp != null) {
                    return emp;
                } else {
                    return asignarEmpleado(listaEmpleados, 'D');
                }
            case 'D':
                for (Iterator<Empleado> iterator = listaEmpleados.iterator(); iterator.hasNext();) {
                    Empleado empleado = iterator.next();
                    if (empleado.getCargoEmpleado().equals(CargoEmpleadoEnum.DIRECTOR)) {
                        if (empleado.isDisponible()) {
                            emp = empleado;
                            iterator.remove();
                            break;
                        }
                    }
                }
                if (emp != null) {
                    return emp;
                }
                break;
            default:
                break;
        }
        return null;
    }
}
