/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.model;

import com.almundo.cache.EmpleadoCache;
import com.almundo.enums.EstadoLLamadaEnum;
import com.almundo.enums.converter.EstadoLLamadaConverter;
import com.almundo.util.FechaUtil;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Emmanuel Robinson
 */
@Entity
@Table(name = "LLAMADA")
public class Llamada implements Serializable {
    
    /**
     * Atributo id, indentificador unico de registro
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ID_LLAMADA")
    @SequenceGenerator(name = "SEQ_ID_LLAMADA", sequenceName = "SEQ_ID_LLAMADA", allocationSize = 1, initialValue = 1)
    @Column(name = "ID", updatable = false)
    private Long id;
    
    /**
     * Atributo numero
     * Numero de telefono origen de la llamada
     */
    @Column(name = "NUMERO", nullable = false)
    private String numero;
    
    /**
     * Atributo fechaIngreso
     * Fecha y hora de ingreso de la llamada
     */
    @Column(name = "FECHA_INGRESO", nullable = true)
    private LocalDateTime fechaIngreso;
    
    /**
     * Atributo fechaIngreso
     * Fecha y hora de ingreso de la llamada
     */
    @Column(name = "FECHA_INICIO", nullable = true)
    private LocalDateTime fechaInicio;
    
    /**
     * Atributo fechaIngreso
     * Fecha y hora de ingreso de la llamada
     */
    @Column(name = "FECHA_FIN", nullable = true)
    private LocalDateTime fechaFin;
    
    /**
     * Atributo duracion
     * Duracion de la llamada expresanda en segundos, si la llamada esta perdida esta valor puede ser null
     */
    @Column(name = "DURACION", nullable = true)
    private Long duracion;
    
    /**
     * Atributo empleado
     * Empleado que atiende la llamada, si la llamada esta perdida este valor puede se null
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_EMPLEADO", foreignKey = @ForeignKey(name = "FK_LLAMADA_EMPLEADO"), nullable = true)
    private Empleado empleado;
    
    /**
     * Atributo estado
     * Estado de la llamada
     */
    @Convert(converter = EstadoLLamadaConverter.class)
    @Column(name = "ESTADO_LLAMADA", nullable = false)
    private EstadoLLamadaEnum estadoLlamada;

    /**
     * Metodo get del atributo id
     * @return Long
     */
    public Long getId() {
        return id;
    }

    /**
     * Metodo set del atributo id
     * @param id 
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     * Metodo get del atributo numero
     * @return String
     */
    public String getNumero() {
        return numero;
    }

    /**
     * Metodo set del atributo numero
     * @param numero 
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }

    public LocalDateTime getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(LocalDateTime fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public LocalDateTime getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(LocalDateTime fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public LocalDateTime getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(LocalDateTime fechaFin) {
        this.fechaFin = fechaFin;
    }

    /**
     * Metodo get del atributo duracion
     * @return duracion
     */
    public Long getDuracion() {
        return duracion;
    }

    /**
     * Metodo set del atributo duracion
     * @param duracion 
     */
    public void setDuracion(Long duracion) {
        this.duracion = duracion;
    }
    
    /**
     * Metodo get del atributo estadoLlamada
     * @return EstadoLLamadaEnum
     */
    public EstadoLLamadaEnum getEstadoLlamada() {    
        return estadoLlamada;
    }

    /**
     * Metodo set del atributo estadoLlamada
     * @param estadoLlamada 
     */
    public void setEstadoLlamada(EstadoLLamadaEnum estadoLlamada) {    
        this.estadoLlamada = estadoLlamada;
    }

    /**
     * Metodo get del atributo empleado
     * @return empleado
     */
    public Empleado getEmpleado() {
        return empleado;
    }

    /**
     * Metodo set del atributo empleado
     * @param empleado 
     */
    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    @Override
    public String toString() {
    	StringBuilder infoCalled = new StringBuilder();
        infoCalled.append("\n ********************************************************************************");
        if(EstadoLLamadaEnum.DEVUELTA.equals(this.estadoLlamada)) {
        	infoCalled.append("\n *                       Detalle de llamada Devuelta                            *");
        }else {
        	infoCalled.append("\n *                       Detalle de llamada Entrante                            *");
        }
        infoCalled.append("\n ********************************************************************************");
        infoCalled.append("\n Fecha y Hora Ingreso de llamada:     ");
        infoCalled.append(FechaUtil.LocalDateTimeToString(FechaUtil.FORMATO_FECHA_HORA, this.getFechaIngreso()));
        infoCalled.append("\n Origen de Llamada:                   ");
        infoCalled.append(this.getNumero());
        infoCalled.append("\n Estado de Llamada:                   ");
        infoCalled.append(this.getEstadoLlamada().getDescripcion());
        infoCalled.append("\n Operador Asignado:                   ");
        infoCalled.append(this.getEmpleado() != null ? this.getEmpleado().getPersona().getNombreCompleto(): "No hay funcionarios disponibles");
        infoCalled.append("\n Cargo Operador:                      ");
        infoCalled.append(this.getEmpleado() != null ? this.getEmpleado().getCargoEmpleado().name(): "");
        if (!this.getEstadoLlamada().equals(EstadoLLamadaEnum.PERDIDAD)) {
        	infoCalled.append("\n Fecha y Hora Inicio de llamada:      ");
            infoCalled.append(FechaUtil.LocalDateTimeToString(FechaUtil.FORMATO_FECHA_HORA, this.getFechaInicio()));
            infoCalled.append("\n Fecha y Hora Fin de llamada:         ");
            infoCalled.append(FechaUtil.LocalDateTimeToString(FechaUtil.FORMATO_FECHA_HORA, this.getFechaFin()));
        }
        infoCalled.append("\n ********************************************************************************");
        return infoCalled.toString();
    }
    
    
    
}
