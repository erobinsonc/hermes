/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.model;

import com.almundo.enums.CargoEmpleadoEnum;
import com.almundo.enums.EstadoActivoInactivoEnum;
import com.almundo.enums.converter.CargoEmpleadoConverter;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Emmanuel Robinson
 */
@Entity
@Table(name = "EMPLEADO")
public class Empleado implements Serializable {

    /**
     * Atributo id, indentificador unico de registro
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ID_EMPLEADO")
    @SequenceGenerator(name = "SEQ_ID_EMPLEADO", sequenceName = "SEQ_ID_EMPLEADO", allocationSize = 1, initialValue = 1)
    @Column(name = "ID", updatable = false)
    private Long id;
    
    /**
     *
     */
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_PERSONA", foreignKey = @ForeignKey(name = "FK_EMPLEADO_PERSONA"), nullable = false)
    private Persona persona;

    /**
     *
     */
    @Convert(converter = CargoEmpleadoConverter.class)
    @Column(name = "CARGO_EMPLEADO", nullable = false)
    private CargoEmpleadoEnum cargoEmpleado;
    
    /**
     * Atributo disponible Estado de los empleados que pueden ser asignados a
     * una llamada
     */
    @Column(name = "DISPONIBLE", nullable = false)
    private boolean disponible;

    /**
     * 
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "ESTADO", nullable = false)
    private EstadoActivoInactivoEnum estado;

    public Empleado() {
    }
    
    /**
     * Contructor de la Entidad Empleado
     * @param numeroDocumento
     * @param primerNombre
     * @param segundoNombre
     * @param primerApellido
     * @param segundoApellido
     * @param cargoEmpleado
     * @param disponible
     * @param estado
     */
    public Empleado(String numeroDocumento, String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, CargoEmpleadoEnum cargoEmpleado, boolean disponible, EstadoActivoInactivoEnum estado) {
        this.persona = new Persona(numeroDocumento, primerNombre, segundoNombre, primerApellido, segundoApellido);
        this.cargoEmpleado = cargoEmpleado;
        this.disponible = disponible;
        this.estado = estado;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public CargoEmpleadoEnum getCargoEmpleado() {
        return cargoEmpleado;
    }

    public void setCargoEmpleado(CargoEmpleadoEnum cargoEmpleado) {
        this.cargoEmpleado = cargoEmpleado;
    }

    public boolean isDisponible() {
        return disponible;
    }

    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }

    public EstadoActivoInactivoEnum getEstado() {
        return estado;
    }

    public void setEstado(EstadoActivoInactivoEnum estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Empleado{" + "id=" + id + ", persona=" + persona + ", cargoEmpleado=" + cargoEmpleado + ", disponible=" + disponible + ", estado=" + estado + '}';
    }
  
}
