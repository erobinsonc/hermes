/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PostLoad;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.springframework.util.ObjectUtils;

/**
 *
 * @author Emmanuel Robinson C.
 */
@Entity
@Table(name = "PERSONA")
public class Persona implements Serializable {

    /**
     * Atributo id, indentificador unico de registro
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ID_PERSONA")
    @SequenceGenerator(name = "SEQ_ID_PERSONA", sequenceName = "SEQ_ID_PERSONA", allocationSize = 1, initialValue = 1)
    @Column(name = "ID", updatable = false)
    private Long id;

    /**
     * Atributo numeroDocumento
     */
    @Column(name = "NUMERO_DOCUMENTO", nullable = true, length = 20)
    private String numeroDocumento;

    /**
     * Atributo nombre Nombre del empleado
     */
    @Column(name = "PRIMER_NOMBRE", nullable = false, length = 30)
    private String primerNombre;

    /**
     * Atributo nombre Nombre del empleado
     */
    @Column(name = "SEGUNDO_NOMBRE", nullable = true, length = 30)
    private String segundoNombre;

    /**
     * Atributo apellido Apellido del empleador
     */
    @Column(name = "PRIMER_APELLIDO", nullable = false, length = 30)
    private String primerApellido;

    /**
     * Atributo apellido Apellido del empleador
     */
    @Column(name = "SEGUNDO_APELLIDO", nullable = true, length = 30)
    private String segundoApellido;

    @Transient
    private String nombreCompleto;

    public Persona() {
    }

    public Persona(String numeroDocumento, String primerNombre, String segundoNombre, String primerApellido, String segundoApellido) {
        this.numeroDocumento = numeroDocumento;
        this.primerNombre = primerNombre;
        this.segundoNombre = segundoNombre;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    @Override
    public String toString() {
        return "Persona{" + "id=" + id + ", numeroDocumento=" + numeroDocumento + ", primerNombre=" + primerNombre + ", segundoNombre=" + segundoNombre + ", primerApellido=" + primerApellido + ", segundoApellido=" + segundoApellido + '}';
    }

    @PostLoad
    private void load() {
        StringBuilder nombre = new StringBuilder();
        nombre.append(primerNombre);
        if(!ObjectUtils.isEmpty(segundoNombre)){
            nombre.append(" ");
            nombre.append(segundoNombre);
        }
        nombre.append(" ");
        nombre.append(primerApellido);
        if(!ObjectUtils.isEmpty(segundoApellido)){
            nombre.append(" ");
            nombre.append(segundoApellido);
        }
        
        nombreCompleto = nombre.toString();
    }

}
