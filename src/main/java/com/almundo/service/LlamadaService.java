/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.service;

import com.almundo.enums.EstadoLLamadaEnum;
import com.almundo.model.Llamada;

/**
 *
 * @author Emmanuel Robinson C.
 */
public interface LlamadaService {
    
    Llamada save(Llamada llamada);
    
    Iterable<Llamada> findAll();
    
    Iterable<Llamada> findByEstadoLlamada(EstadoLLamadaEnum estadoLlamada);
    
}
