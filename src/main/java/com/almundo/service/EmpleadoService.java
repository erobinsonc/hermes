package com.almundo.service;

import com.almundo.enums.EstadoActivoInactivoEnum;
import com.almundo.model.Empleado;
import com.almundo.model.Persona;

/**
 *
 * @author Emmanuel Robinson C.
 */
public interface EmpleadoService {
    
    Iterable<Persona> findAllPersona();
    
    Iterable<Empleado> findByEstado(EstadoActivoInactivoEnum activoInactivo);
    
    Empleado save(Empleado empleado);
    
}
