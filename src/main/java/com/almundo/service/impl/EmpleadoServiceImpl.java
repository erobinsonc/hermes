/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.service.impl;

import com.almundo.enums.EstadoActivoInactivoEnum;
import com.almundo.model.Empleado;
import com.almundo.model.Persona;
import com.almundo.repository.EmpleadoRepository;
import com.almundo.repository.PersonaRepository;
import com.almundo.service.EmpleadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Emmanuel Robinson C.
 */
@Service
public class EmpleadoServiceImpl implements EmpleadoService{
    
    @Autowired(required = true)
    private PersonaRepository personaRepository;
    
    @Autowired(required = true)
    private EmpleadoRepository empleadoRepository;
    
    @Override
    public Iterable<Persona> findAllPersona(){
        return personaRepository.findAll();
    }
    
    @Override
    public Iterable<Empleado> findByEstado(EstadoActivoInactivoEnum activoInactivo){
        return empleadoRepository.findByEstado(activoInactivo);
    }
    
    @Override
    public Empleado save(Empleado empleado){
        return empleadoRepository.save(empleado);
    }
    
}
