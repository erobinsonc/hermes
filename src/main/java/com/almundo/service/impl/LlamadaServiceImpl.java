/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.service.impl;

import com.almundo.enums.EstadoLLamadaEnum;
import com.almundo.model.Llamada;
import com.almundo.repository.LlamadaRepository;
import com.almundo.service.LlamadaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Emmanuel Robinson C.
 */
@Service
public class LlamadaServiceImpl implements LlamadaService{
    
    @Autowired
    private LlamadaRepository llamadaRepository;
    
    @Override
    public synchronized Llamada save(Llamada llamada){
        return llamadaRepository.save(llamada);
    }
    
    @Override
    public Iterable<Llamada> findAll(){
        return llamadaRepository.findAll();
    }
    
    @Override
    public Iterable<Llamada> findByEstadoLlamada(EstadoLLamadaEnum estadoLlamada){
    	return llamadaRepository.findByEstadoLlamadaOrderByFechaIngresoAsc(estadoLlamada);
    }
    
}
