/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.enums;

/**
 *
 * @author Emmanuel Robinson
 */
public enum EstadoLLamadaEnum {
    
    /**
     * Atributos de la enumeracion
     */
    ATENDIDAD("A", "Atendida"),
    DEVUELTA("D", "Devuelta"),
    PERDIDAD("P", "Perdida");
    
    /**
     * Variable codigo
     */
    private String codigo;
    
    /**
     * Variable descripcion
     */
    private String descripcion;
    
    /**
     * Contructor de la enumeracion EstadoLLamadaEnum
     * @param codigo
     * @param descripcion  
     */
    private EstadoLLamadaEnum(String codigo, String descripcion){
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    /**
     * Metodo que obtiene el codigo asociado a la enumeracion
     * @return String
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Metodo que obtiene la descripcion asociada a la enumeracion
     * @return String
     */
    public String getDescripcion() {
        return descripcion;
    }
    
    public static EstadoLLamadaEnum obtenerEnumXCodigo(String codigo) {
        for (EstadoLLamadaEnum estadoLLamada : EstadoLLamadaEnum.values()) {
            if (estadoLLamada.getCodigo().equals(codigo)) {
                return estadoLLamada;
            }
        }
        throw new IllegalArgumentException("No se encontró un EstadoLLamada con código [" + codigo + "].");
    }
    
}
