/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.enums.converter;

import com.almundo.enums.EstadoLLamadaEnum;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import org.springframework.util.ObjectUtils;

/**
 *
 * @author Emmanuel Robinson C.
 */
@Converter(autoApply = true)
public class EstadoLLamadaConverter implements AttributeConverter<EstadoLLamadaEnum, String>{

    @Override
    public String convertToDatabaseColumn(EstadoLLamadaEnum estadoLLamada) {
        if (ObjectUtils.isEmpty(estadoLLamada)) {
            return null;
        }
        return estadoLLamada.getCodigo();
    }

    @Override
    public EstadoLLamadaEnum convertToEntityAttribute(String codigo) {
        return EstadoLLamadaEnum.obtenerEnumXCodigo(codigo);
    }
    
}
