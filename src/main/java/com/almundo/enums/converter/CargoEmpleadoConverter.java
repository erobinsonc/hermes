/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.enums.converter;

import com.almundo.enums.CargoEmpleadoEnum;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import org.springframework.util.ObjectUtils;

/**
 *
 * @author Emmanuel Robinson C.
 */
@Converter(autoApply = true)
public class CargoEmpleadoConverter implements AttributeConverter<CargoEmpleadoEnum, String> {

    @Override
    public String convertToDatabaseColumn(CargoEmpleadoEnum cargoEmpleado) {
        if (ObjectUtils.isEmpty(cargoEmpleado)) {
            return null;
        }
        return cargoEmpleado.getCodigo();
    }

    @Override
    public CargoEmpleadoEnum convertToEntityAttribute(String codigo) {
        return CargoEmpleadoEnum.obtenerEnumXCodigo(codigo);
    }

}