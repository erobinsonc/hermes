/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.enums;

/**
 *
 * @author Emmanuel Robinson C.
 */
public enum CargoEmpleadoEnum {

    OPERADOR("O"),
    SUPERVISOR("S"),
    DIRECTOR("D");

    private String codigo;

    private CargoEmpleadoEnum(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

    public static CargoEmpleadoEnum obtenerEnumXCodigo(String codigo) {
        for (CargoEmpleadoEnum cargoEmpleado : CargoEmpleadoEnum.values()) {
            if (cargoEmpleado.getCodigo().equals(codigo)) {
                return cargoEmpleado;
            }
        }
        throw new IllegalArgumentException("No se encontró un CargoEmpleado con código [" + codigo + "].");
    }
}
