package com.almundo.config;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.IterableUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.util.ObjectUtils;

import com.almundo.cache.EmpleadoCache;
import com.almundo.enums.CargoEmpleadoEnum;
import com.almundo.enums.EstadoActivoInactivoEnum;
import com.almundo.hilos.DispatcherFilterThread;
import com.almundo.hilos.ValidateMissedCallFilterThread;
import com.almundo.model.Empleado;
import com.almundo.model.Llamada;
import com.almundo.service.EmpleadoService;

@SpringBootApplication
@ComponentScan("com.almundo")
@EntityScan(basePackages = "com.almundo.model")
@EnableJpaRepositories("com.almundo.repository")
@PropertySource("classpath:almundo.properties")
public class AlmundoApplication {

	private static final Logger LOGGER = LogManager.getLogger(AlmundoApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(AlmundoApplication.class);
	}

	@Value("${cantidadLlamadasConcurrentes}")
	private int CANTIDAD_LLAMADAS_CONCURRENTES;
	
	@Value("${rangoTiempoValidacionLLamada}")
	private Long RANGO_TIEMPO;
	

	@Autowired
	private EmpleadoService empleadoService;
	
	@Bean
	public CommandLineRunner demo() {
		return (args) -> {
			this.guardarEmpleados();

			Iterable<Empleado> empleados = empleadoService.findByEstado(EstadoActivoInactivoEnum.ACTIVO);
			if (!ObjectUtils.isEmpty(empleados)) {
				// Carga en la cache los empleados configurados
				EmpleadoCache.getInstance().cargarEmpleados(IterableUtils.toList(empleados));

			}

			// Valida llamadas perdidas para realizar devolucion de la misma
			ValidateMissedCallFilterThread missedCall = new ValidateMissedCallFilterThread((RANGO_TIEMPO * 1000 * 60));
			missedCall.start();
			
			// Captura llamadas e invoca funcionalidad para su repeccion
			DispatcherFilterThread dispatcher = new DispatcherFilterThread(CANTIDAD_LLAMADAS_CONCURRENTES, llamadasEntrante());
			dispatcher.start();
		};
	}

	/**
	 * Metodo que permite registrar los empleados que pueden antender la llamada. 
	 */
	private void guardarEmpleados() {
		empleadoService.save(new Empleado(null, "Emmanuel", null, "Robinson", "Castellar", CargoEmpleadoEnum.OPERADOR,
				true, EstadoActivoInactivoEnum.ACTIVO));
		empleadoService.save(new Empleado(null, "Dayana", "Carolina", "Pineda", "Cuello", CargoEmpleadoEnum.OPERADOR,
				true, EstadoActivoInactivoEnum.ACTIVO));
		empleadoService.save(new Empleado(null, "Gabriela", null, "Dangond", "Pineda", CargoEmpleadoEnum.OPERADOR, true,
				EstadoActivoInactivoEnum.ACTIVO));
		empleadoService.save(new Empleado(null, "Jhonatan", null, "Castellar", null, CargoEmpleadoEnum.OPERADOR, true,
				EstadoActivoInactivoEnum.ACTIVO));
		empleadoService.save(new Empleado(null, "Danilo", null, "Hernandez", "Pereira", CargoEmpleadoEnum.SUPERVISOR,
				true, EstadoActivoInactivoEnum.ACTIVO));
		empleadoService.save(new Empleado(null, "Jose", "Alejandro", "Villalba", "Vimos", CargoEmpleadoEnum.SUPERVISOR,
				true, EstadoActivoInactivoEnum.ACTIVO));
		empleadoService.save(new Empleado(null, "Guillermo", null, "Diaz", null, CargoEmpleadoEnum.SUPERVISOR, false,
				EstadoActivoInactivoEnum.ACTIVO));
		empleadoService.save(new Empleado(null, "Deisy", null, "Castellar", "Tarrifa", CargoEmpleadoEnum.DIRECTOR, true,
				EstadoActivoInactivoEnum.ACTIVO));
	}

	/**
	 * Metodo que obtiene las llamadas entrantes, si se requiere un numero mayor de
	 * llamadas se pueden adicionar a la lista
	 *
	 * @return List<Llamada>
	 */
	private static List<Llamada> llamadasEntrante() {
		List<Llamada> listaLlamadas = new ArrayList<Llamada>();
		Llamada c1 = new Llamada();
		c1.setNumero("57301235674");

		Llamada c2 = new Llamada();
		c2.setNumero("573151946521");

		Llamada c3 = new Llamada();
		c3.setNumero("0316620135");

		Llamada c4 = new Llamada();
		c4.setNumero("573125612345");

		Llamada c5 = new Llamada();
		c5.setNumero("0356621457");

		Llamada c6 = new Llamada();
		c6.setNumero("03566231458");

		Llamada c7 = new Llamada();
		c7.setNumero("573056512322");

		Llamada c8 = new Llamada();
		c8.setNumero("573012623222");

		Llamada c9 = new Llamada();
		c9.setNumero("573102020103");

		Llamada c10 = new Llamada();
		c10.setNumero("0317712354");

		listaLlamadas.add(c1);
		listaLlamadas.add(c2);
		listaLlamadas.add(c3);
		listaLlamadas.add(c4);
		listaLlamadas.add(c5);
		listaLlamadas.add(c6);
		listaLlamadas.add(c7);
		listaLlamadas.add(c8);
		listaLlamadas.add(c9);
		listaLlamadas.add(c10);

		return listaLlamadas;

	}

}
