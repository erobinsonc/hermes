/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.repository;

import com.almundo.enums.EstadoLLamadaEnum;
import com.almundo.model.Llamada;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Emmanuel Robinson C.
 */
@Repository
public interface LlamadaRepository extends PagingAndSortingRepository<Llamada, Long>{
    
	Iterable<Llamada> findByEstadoLlamadaOrderByFechaIngresoAsc(EstadoLLamadaEnum estadoLlamada);
}
