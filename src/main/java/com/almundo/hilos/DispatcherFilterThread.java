/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.hilos;

import com.almundo.model.Llamada;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Emmanuel Robinson
 */
public class DispatcherFilterThread extends Thread {

    /**
     * Variable LOGGER A traves de esta se puede realizar el registro en el log
     */
    private final static Logger LOGGER = LogManager.getLogger(DispatcherFilterThread.class);

    /**
     * Atributo ExecutorService Encargado de gestionar los multiples hilos
     */
    private ExecutorService executorService;

    /**
     * Lista de llamadas entrantes
     */
    private List<Llamada> listaLlamadas;

    /**
     * Constructor de la clase
     * @param numeroHilos
     * @param listaLlamadas
     */
    public DispatcherFilterThread(int numeroHilos, List<Llamada> listaLlamadas) {
        this.executorService = Executors.newFixedThreadPool(numeroHilos);
        this.listaLlamadas = listaLlamadas;
    }

    /**
     * Metodo que se encarga de hacer la distrubucion de llamdas
     */
    @Override
    public void run() {
        try {
            for (Llamada call : listaLlamadas) {
                Dispatcher dispatcher = new Dispatcher(call);
                executorService.execute(dispatcher);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

}
