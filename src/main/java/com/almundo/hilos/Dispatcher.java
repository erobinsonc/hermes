/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.hilos;

import java.time.LocalDateTime;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.almundo.cache.EmpleadoCache;
import com.almundo.enums.EstadoLLamadaEnum;
import com.almundo.model.Empleado;
import com.almundo.model.Llamada;
import com.almundo.provider.ApplicationContextProvider;
import com.almundo.service.LlamadaService;
import com.almundo.util.Utilidades;

/**
 *
 * @author Emmanuel Robinson
 */
public class Dispatcher implements Runnable {

    /**
     * Variable LOGGER A traves de esta se puede realizar el registro en el log
     */
    private static final Logger LOGGER = LogManager.getLogger(Dispatcher.class);

    /**
     * Varibale call Llamada que se esta procesando
     */
    private Llamada llamada;

	/**
     * Constructor de la clase Dispatcher
     *
     * @param llamada
     */
    public Dispatcher(Llamada llamada) {
        this.llamada = llamada;
    }
    
    /**
     * Metodo que se encarga de procesar la llamda
     */
    @Override
    public void run() {
        try {
            this.dispatchCall();
            this.guardarLlamada(llamada);            
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }
    
    /**
     * 
     * @param llamada
     */
    private void guardarLlamada(Llamada llamada) {
    	LlamadaService llamadaService = ApplicationContextProvider.getApplicationContext().getBean(LlamadaService.class);
        llamadaService.save(llamada);
        LOGGER.info(llamada.toString());
    }

    /**
     * Metodo que asigna un empleado a un llamda entrante
     */
    private void dispatchCall() {
        Empleado empleado = EmpleadoCache.getInstance().asignarEmpleado(EmpleadoCache.getInstance().consultarEmpleados(), 'O');
        llamada.setFechaIngreso(LocalDateTime.now());
        if (empleado != null) {
            llamada.setEmpleado(empleado);
            llamada.setEstadoLlamada(EstadoLLamadaEnum.ATENDIDAD);
            llamada.setFechaInicio(LocalDateTime.now());
        	llamada.setDuracion(Utilidades.durationCall());
        	llamada.setFechaFin(LocalDateTime.now());
            
        	//Vuelve a colocar disponible empleado que atendio la llamda
            EmpleadoCache.getInstance().adicionarEmpleado(empleado);
        } else {
            llamada.setEstadoLlamada(EstadoLLamadaEnum.PERDIDAD);
        }
    }
}
