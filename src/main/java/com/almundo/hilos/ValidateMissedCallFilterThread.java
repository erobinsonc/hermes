package com.almundo.hilos;

import java.time.LocalDateTime;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.ObjectUtils;

import com.almundo.cache.EmpleadoCache;
import com.almundo.enums.EstadoLLamadaEnum;
import com.almundo.model.Empleado;
import com.almundo.model.Llamada;
import com.almundo.provider.ApplicationContextProvider;
import com.almundo.service.LlamadaService;
import com.almundo.util.Utilidades;

public class ValidateMissedCallFilterThread extends Thread{
	
	private static final Logger LOGGER = LogManager.getLogger(ValidateMissedCallFilterThread.class);
	
	private Long INTERVALO_COMPROBACION;
	
	private LlamadaService llamadaService;
	
	public ValidateMissedCallFilterThread(Long iNTERVALO_COMPROBACION) {
		super();
		this.INTERVALO_COMPROBACION = iNTERVALO_COMPROBACION;
		this.llamadaService = ApplicationContextProvider.getApplicationContext().getBean(LlamadaService.class);
	}

	@Override
    public void run() {
        while (true) {
        	Iterable<Llamada> listaLlamadasPerdidas = getLLamadasPerdidas();
        	if(ObjectUtils.isEmpty(listaLlamadasPerdidas)) {
                try {
                	LOGGER.info("No hay llamadas pendientes por devolver");
					Thread.sleep(INTERVALO_COMPROBACION);
				} catch (InterruptedException e) {
					LOGGER.error(e.getMessage());
				}
        	}else {
        		for (Llamada llamada : listaLlamadasPerdidas) {
        			Empleado empleado = EmpleadoCache.getInstance().asignarEmpleado(EmpleadoCache.getInstance().consultarEmpleados(), 'O');
                    if (empleado != null) {
                    	llamada.setFechaInicio(LocalDateTime.now());
                    	llamada.setDuracion(Utilidades.durationCall());
                    	llamada.setFechaFin(LocalDateTime.now());
                    	llamada.setEmpleado(empleado);
                    	llamada.setEstadoLlamada(EstadoLLamadaEnum.DEVUELTA);
                    	
                    	//Vuelve a colocar disponible empleado que devolvio la llamda
                        EmpleadoCache.getInstance().adicionarEmpleado(llamada.getEmpleado());
                        actualizarLlamada(llamada);
                        LOGGER.info(llamada.toString());
                    } 
				}
        	}
        }
    }
	
	private synchronized Iterable<Llamada> getLLamadasPerdidas() {
//		LlamadaService llamadaService = ApplicationContextProvider.getApplicationContext().getBean(LlamadaService.class);
        return llamadaService.findByEstadoLlamada(EstadoLLamadaEnum.PERDIDAD);
    }
	
	private synchronized void actualizarLlamada(Llamada llamada) {
//		LlamadaService llamadaService = ApplicationContextProvider.getApplicationContext().getBean(LlamadaService.class);
        llamadaService.save(llamada);
    }

}
