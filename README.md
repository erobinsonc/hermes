#Registro de Informacion:
El sistema cuenta con dos métodos en la clase AlmundoApplication que permiten registrar empleados y configurar las llamadas que va a recibir y procesar la aplicación. 

- guardarEmpleados() 
- llamadasEntrante()

#Log:
La aplicación maneja la implementación Log4j2, la cual muestra la traza de proceso en la consola como también en un archivo denominado almundo.log en cual se encuentra en la carpeta /logs

#Ejecucion:
La aplicación fue construida utilizando el framework Spring Boot, solo es necesario descargar el proyecto y ejecutar. Maneja una base de datos H2 para el almacenamiento temporal de la información.

EL número de llamadas permitidas de manera concurrente se encuentra configurado en el archivo almundo.properties

#Procesos:
El sistema cuenta con dos Hilos, los cuales cumplen funciones específicas

- ValidateMissedCallFilterThread: Valida las llamadas que quedaron perdidas, revisando si se encuentra un empleado disponible para devolver la llamada al usuario. De ser asi realiza este proceso. 

- DispatcherFilterThread: Recepción de llamadas y asignación a la clase Dispatcher para que se encargue de su procesamiento dependiendo la disponibilidad de empleados. 


#Extras/Plus:
El sistema registra en base de datos el detalle de las llamadas perdidas, luego a través del hilo de validación de estas llamadas verifica que existan ya empleados disponibles para procesar estas llamadas. Realizando si es posible la devaluación de la misma. 
